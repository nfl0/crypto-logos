from PIL import Image

image = Image.open('icon.png')

print(image.size)

new_image1 = image.resize((16, 16), resample=Image.LANCZOS)
new_image2 = image.resize((32, 32), resample=Image.LANCZOS)
new_image3 = image.resize((48, 48), resample=Image.LANCZOS)
new_image4 = image.resize((64, 64), resample=Image.LANCZOS)
new_image5 = image.resize((128, 128), resample=Image.LANCZOS)

new_image1.save('icon-16.png')
new_image2.save('icon-32.png')
new_image3.save('icon-48.png')
new_image4.save('icon-64.png')
new_image5.save('icon-128.png')
 
